import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { DatePipe } from '@angular/common';

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { ClientPage } from '../pages/client/client';
import { ConfigurationPage } from '../pages/configuration/configuration';
import { ControlTemplatePage } from '../pages/control-template/control-template';
import { StateRegisterPage } from '../pages/state-register/state-register'
import { MonitorPage } from '../pages/monitor/monitor';
import { TemplateTypePage } from '../pages/template-type/template-type';
import { DetailTemplatePage } from '../pages/detail-template/detail-template';
import { StartTrapPage} from '../pages/start-trap/start-trap'
import { MapsModal } from '../pages/maps-modal/maps-modal';


import { AuthService } from '../service/auth.service';
import { ClientService } from '../service/client.service';
import { MonitorService } from '../service/monitor.service';
import { TrapTypeService } from '../service/trapType.service';
import { ControlTemplateService } from '../service/controlTemplate.service';
import { StateRegisterService } from '../service/stateRegister.service';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    ClientPage,
    ConfigurationPage,
    ControlTemplatePage,
    StateRegisterPage,
    MonitorPage,
    TemplateTypePage,
    DetailTemplatePage,
    StartTrapPage,
    MapsModal
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    ClientPage,
    ConfigurationPage,
    ControlTemplatePage,
    StateRegisterPage,
    MonitorPage,
    TemplateTypePage,
    DetailTemplatePage,
    StartTrapPage,
    MapsModal
  ],
  providers: [
    AuthService,
    ClientService,
    MonitorService,
    TrapTypeService,
    ControlTemplateService,
    StateRegisterService,
    StatusBar,
    DatePipe,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
