import { Injectable } from '@angular/core';
import { Http, Response} from'@angular/http';
import { Headers, RequestOptions } from'@angular/http';

import { StateTrap } from '../class/stateTrap';
import { StateRegister } from '../class/stateRegister';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/catch';

@Injectable()
export class StateRegisterService {
    private options;
    private url = 'http://192.168.0.16:8080/';

    constructor(private http : Http){
        let token = localStorage.getItem('token');
        let headersAux = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        });
        this.options = new RequestOptions({ headers: headersAux});
    }

    getStatesTrap(idTrapType: number): Observable<StateTrap[]>
    {
        let url = `${this.url}stateTrap/${idTrapType}`
        return this.http.get(url, this.options)
                .map(r => r.json())
                .catch(this.handleError);
    }

    getAllStateRegisters(idControlTemplate: number): Observable<StateRegister[]>
    {
        let url = `${this.url}stateRegister/${idControlTemplate}`
        return this.http.get(url, this.options)
                .map(r => r.json())
                .catch(this.handleError);
    }

    updateStateRegister(stateRegister: StateRegister)
    {
        let url = `${this.url}stateRegister/`;
        let stateRegisterJson = JSON.stringify(stateRegister);        
        return this.http.put(url, stateRegisterJson, this.options)
                .map(r => r.json())
                .catch(this.handleError);
    }

    updateStateVisit(idControlTemplate: number, f: string, a: string)
    {
        let url = `${this.url}visit/`;
        let updateVisitArray = {id_control_template: idControlTemplate, findings:f, actions:a};
        let updateVisitJson = JSON.stringify(updateVisitArray); 
        return this.http.put(url, updateVisitJson, this.options)
            .map(r => r.json())
            .catch(this.handleError);
    }

    private handleError(error: Response | any)
    {
        let errMsg: string;
        if(error instanceof Response) {
            let bodyError = error.json() || '';
            let err = bodyError.error || JSON.stringify(bodyError);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Observable.throw(errMsg);
    }
}