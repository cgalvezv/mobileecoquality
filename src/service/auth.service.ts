import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
    rut: string;
    loggedIn: boolean;
    url = 'http://192.168.0.16:8080/auth';

    constructor(private http: Http){
        this.rut = '';
        this.loggedIn = false;
    }

    login(credentials){
        let url = `${this.url}/login`;
        let userJson = JSON.stringify(credentials);

        return this.http.post(url, userJson, {            
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        })
        .map(result => result.text())
        .map(result => {
                if (result == "error" || result == "noFound") {
                    this.loggedIn = false;
                } else {
                    localStorage.setItem('token',result);                 
                    localStorage.setItem('rut_session', credentials.rut);
                    this.rut = credentials.rut;
                    this.loggedIn = true;
                }
                return this.loggedIn;
        });
    }

    logout(): void {
        localStorage.removeItem('token');
        localStorage.removeItem('rut_session');
        localStorage.removeItem('fullname_session');        
        this.rut = '';
        this.loggedIn = false;
        console.log('Sesion cerrada');
    }

    isLoggedIn(){
        return this.loggedIn;
    }
}