import { Injectable } from '@angular/core';
import { Http, Response} from'@angular/http';
import { Headers, RequestOptions } from'@angular/http';

import { TrapType } from '../class/trapType';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/catch';

@Injectable()
export class TrapTypeService{
    private options;
    private url = 'http://192.168.0.16:8080/trapType';

    constructor(private http : Http) {
        let token = localStorage.getItem('token');
        let headersAux = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        });
        this.options = new RequestOptions({ headers: headersAux});
    }

    getAllTrapTypes(date, idClient): Observable<TrapType[]>{
        let url = `${this.url}/${date}/${idClient}`
        return this.http.get(url, this.options)
                .map(r => r.json())
                .catch(this.handleError);;
    }

    private handleError(error: Response | any)
    {
        let errMsg: string;
        if(error instanceof Response) {
            let bodyError = error.json() || '';
            let err = bodyError.error || JSON.stringify(bodyError);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Observable.throw(errMsg);
    }
}
