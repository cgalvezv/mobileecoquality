export class StateRegister{
    constructor(
        public id: number,
        public id_trap: number,
        public id_control_template: number,
        public id_trap_state: number,
        public id_trap_location: number,        
        public trap_number: number,
        public trap_letter: string,
        public is_registered: boolean
    ){}
}