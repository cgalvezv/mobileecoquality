export class Monitor{
    constructor(
        public rut: string,        
        public email: string,
        public username: string,        
        public name: string,
        public last_name: string,
        public address: string,
        public phone_number: string
    ){}
}