export class ControlTemplate{
    constructor(
        public id: number,
        public id_visit: number,
        public id_client: number,
        public id_trap_type: number,
        public total_trap: number,        
        public visit_date: string,
        public findings: Text,
        public actions: Text
    ){}
}