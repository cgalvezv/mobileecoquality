import { StateRegister } from '../class/stateRegister';

export class BodyMail{
    constructor(
        public statesRegisters: StateRegister[],
        public findings: Text,
        public actions: Text
    ){}
}