export class TrapType{
    constructor(
        public id: number,
        public template_name: string,
        public trap_type_name: string
    ){}
}