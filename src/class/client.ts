export class Client{
    constructor(
        public id: number,
        public name: string,
        public physical_place: string,
        public  url_plan_location: string
    ){}
}