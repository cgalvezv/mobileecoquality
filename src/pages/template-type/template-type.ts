import { Component } from '@angular/core';
import { DatePipe } from '@angular/common';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

import { DetailTemplatePage } from '../detail-template/detail-template';

import { TrapTypeService } from '../../service/trapType.service';
import { TrapType } from '../../class/trapType';
import { Client } from '../../class/client';

@IonicPage()
@Component({
  selector: 'page-template-type',
  templateUrl: 'template-type.html',
})
export class TemplateTypePage {
  trapTypes: TrapType[];
  lengthTrapTypes: number;
  username: string;
  client: Client;
  current_date: Date;
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private loadingCtrl: LoadingController,
              private trapTypeSrv: TrapTypeService) {
      this.client = this.navParams.get('client');
      this.current_date = this.navParams.get('current_date');
      this.loadAllTrapTypes(this.client.id, this.current_date);
      this.lengthTrapTypes = this.trapTypes.length;
  }

  loadAllTrapTypes(idClient: number, current_date: Date){
    var datePipe = new DatePipe('short');
    var date = datePipe.transform(current_date, "yyyy-MM-dd");
    this.trapTypeSrv.getAllTrapTypes(date,idClient)
      .subscribe(
          rs => this.trapTypes = rs,
          er => console.log(er),
          () => {}
       )
  }

  toDetailTemplatePage(trapType: TrapType) 
  {
    this.loadingDetailTemplate();
    this.navCtrl.push(DetailTemplatePage,
    {
      client: this.client,
      trapType: trapType
    });
  }

  private loadingDetailTemplate() {
    let loader = this.loadingCtrl.create({
      content: "Cargando detalles de la plantilla...",
      duration: 2000
    });
    loader.present();
  }
}
