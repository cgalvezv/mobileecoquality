import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TemplateTypePage } from './template-type';

@NgModule({
  declarations: [
    TemplateTypePage,
  ],
  imports: [
    IonicPageModule.forChild(TemplateTypePage),
  ],
  exports: [
    TemplateTypePage
  ]
})
export class TemplateTypeModule {}
