import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { MonitorService } from '../../service/monitor.service';
import { AuthService } from '../../service/auth.service';

import { LoginPage } from '../login/login';
import { Monitor } from '../../class/monitor';


@IonicPage()
@Component({
  selector: 'page-monitor',
  templateUrl: 'monitor.html',
})
export class MonitorPage {

    rut: string;
    monitor: Monitor;

   constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private alertCtrl: AlertController,
              private monitorSrv: MonitorService,
              private auth: AuthService) {
        this.rut = localStorage.getItem('rut_session');
        this.loadMonitorInfo(this.rut);
  }

  loadMonitorInfo(rut)
  {
      console.log(rut);
      this.monitorSrv.getMonitor(rut)
      .subscribe(
        rs => this.monitor = rs[0],
        er => console.log(er),
        () => console.log(this.monitor)
      )
  }

  Logout()
  {
      this.confirmLogout();
  }

  private confirmLogout() {
    let confirm = this.alertCtrl.create({
      title: 'Cerrar Sesión',
      message: '¿Esta seguro de cerrar sesión?',
      buttons: [
        {
          text: 'No',
          handler: () => {              
          }
        },
        {
          text: 'Sí',
          handler: () => {
            this.auth.logout();
            this.navCtrl.setRoot(LoginPage)
              .then(data => console.log(data),
                    error => console.log(error));
          }
        }
      ]
    });
    confirm.present();
  }
}
