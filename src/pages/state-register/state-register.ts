import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { StateRegisterService } from '../../service/stateRegister.service';
import { StateTrap } from '../../class/stateTrap';
import { StateRegister } from '../../class/stateRegister';

@IonicPage()
@Component({
  selector: 'page-state-register',
  templateUrl: 'state-register.html',
})
export class StateRegisterPage {

  idTrap: number;
  trapType: number;
  statesTrap: StateTrap[];
  stateRegister: StateRegister;
  id_trap_state: number;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private stateRegisterSrv: StateRegisterService) {
    this.idTrap = this.navParams.get('idTrap');
    this.stateRegister = this.navParams.get('stateRegister');
    this.trapType = Number(localStorage.getItem('id_trap_type'));
    this.getStateTrap(this.trapType);
  }
  
  updateRegister()
  {    
    this.stateRegister.id_trap_state = Number(this.id_trap_state);
    this.stateRegister.is_registered = true;
    
    this.stateRegisterSrv.updateStateRegister(this.stateRegister)
      .subscribe(
        rs => console.log('Updated!!'),
        er => console.log(er),
        () => console.log('')
    )
    this.navCtrl.pop();
  }

  getStateTrap(idTrapType)
  {
    this.stateRegisterSrv.getStatesTrap(idTrapType)
    .subscribe(
        rs => this.statesTrap = rs,
        er => console.log(er),
        () => {}
      ) 
  }

  setBackgroundColor(){
    let classes;
    console.log(this.trapType);
    if (this.trapType == 1) {
      classes = {
        ceboTemplate: true,
        tcvTemplate: false
      }  
    } else {
      classes = {
        ceboTemplate: false,
        tcvTemplate: true
      } 
    }
    return classes;
  }

  setBackgroundToolbar(){
    if (this.trapType == 1) {
      return 'cebo';
    }
    return 'tcv';
  }
}
