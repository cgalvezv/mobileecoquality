import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StateRegisterPage } from './state-register';

@NgModule({
  declarations: [
    StateRegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(StateRegisterPage),
  ],
  exports: [
    StateRegisterPage
  ]
})
export class StateRegisterModule {}
