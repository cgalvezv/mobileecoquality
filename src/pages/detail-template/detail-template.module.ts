import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailTemplatePage } from './detail-template';

@NgModule({
  declarations: [
    DetailTemplatePage,
  ],
  imports: [
    IonicPageModule.forChild(DetailTemplatePage),
  ],
  exports: [
    DetailTemplatePage
  ]
})
export class DetailTemplateModule {}
