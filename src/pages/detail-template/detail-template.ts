import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform, ActionSheetController } from 'ionic-angular';

import { ControlTemplateService } from '../../service/controlTemplate.service';
import { MonitorService } from '../../service/monitor.service';
import { StateRegisterService } from '../../service/stateRegister.service';

import { ControlTemplate } from '../../class/controlTemplate';
import { StateRegister } from '../../class/stateRegister';
import { Monitor } from '../../class/monitor';
import { TrapType } from '../../class/trapType';
import { Client } from '../../class/client';

import { StartTrapPage } from '../start-trap/start-trap';
import { ControlTemplatePage } from '../control-template/control-template';

@IonicPage()
@Component({
  selector: 'page-detail-template',
  templateUrl: 'detail-template.html',
})
export class DetailTemplatePage {

  controlTemplate: ControlTemplate;
  monitor: Monitor;
  client: Client;
  trapType: TrapType;
  statesRegisters: StateRegister[];

  constructor(public platform: Platform,
              public navCtrl: NavController,
              public navParams: NavParams,
              private alertCtrl: AlertController,
              public actionsheetCtrl:ActionSheetController,
              private controlTemplateSrv: ControlTemplateService,
              private monitorSrv: MonitorService,
              private stateRegisterSrv: StateRegisterService) {
        
        this.client = this.navParams.get('client');
        this.trapType = this.navParams.get('trapType')
        this.loadDetailControlTemplate(this.client.id, this.trapType.id); 
        this.loadMonitorInfo(localStorage.getItem('rut_session'));                
  }

  loadDetailControlTemplate(idClient, idTrapType)
  {
    this.controlTemplateSrv.getControlTemplate(idClient, idTrapType)
      .subscribe(
        rs => this.controlTemplate = rs[0],
        er => console.log(er),
        () => {
          localStorage.setItem('id_control_template',this.controlTemplate.id.toString())
        }
      ) 
  }

  loadMonitorInfo(rut)
  {
    this.monitorSrv.getMonitor(rut)
    .subscribe(
        rs => this.monitor = rs[0],
        er => console.log(er),
        () => {}
      )
  }
  
  loadStatesRegistersTrapId(idControlTemplate)
  {
    this.stateRegisterSrv.getAllStateRegisters(idControlTemplate)
    .subscribe(
      rs => this.statesRegisters = rs,
      er => console.log(er),
      () => {       
        if (this.statesRegisters.length > 0) {
          localStorage.setItem('id_trap_type', this.trapType.id.toString())
          localStorage.setItem("id_control_type", this.controlTemplate.id.toString());
          localStorage.setItem("total_trap", this.controlTemplate.total_trap.toString());
          localStorage.setItem("visit_date", this.controlTemplate.visit_date);
          if (this.trapType.id == 1) {
            this.navCtrl.push(StartTrapPage, {
              statesRegisters: this.statesRegisters
            }); 
          } else {
            this.navCtrl.push(ControlTemplatePage, {
              statesRegisters: this.statesRegisters
            });
          }          
        } else {
          this.deniedAccess();
        }       
      }
    )    
  }

  deniedAccess() {
    let alert = this.alertCtrl.create({
      title: 'Iniciar Plantilla De Control',
      subTitle: 'No se puede accerder, ya que no existen registros de estado para esta plantilla.',
      buttons: ['OK']
    });
    alert.present();
  }

  toStartTemplate()
  {
    let sendActionSheet = this.actionsheetCtrl.create({
      title: '¿Desea iniciar la plantilla de control?',
      cssClass: 'action-sheets-send',
      buttons: [
        {
          text: 'Si',
          role: 'confirm',
          icon: !this.platform.is('ios') ? 'checkmark': null,
          handler: () => {
            this.loadStatesRegistersTrapId(localStorage.getItem('id_control_template'));            
          }
       },
       {
          text: 'No',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,          
          handler: () => {            
          }
       }
      ]
    });
    sendActionSheet.present();
  }

  setBackgroundColor(){
    let classes;
    if (this.trapType.id == 1) {
      classes = {
        ceboTemplate: true,
        tcvTemplate: false
      }  
    } else {
      classes = {
        ceboTemplate: false,
        tcvTemplate: true
      } 
    }
    return classes;
  }

  setBackgroundToolbar(){
    if (this.trapType.id == 1) {
      return 'cebo';
    }
    return 'tcv';
  }
}
