import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapsModal } from './maps-modal';

@NgModule({
  declarations: [
    MapsModal,
  ],
  imports: [
    IonicPageModule.forChild(MapsModal),
  ],
  exports: [
    MapsModal
  ]
})
export class MapsModalModule {}
