import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-maps-modal',
  templateUrl: 'maps-modal.html',
})
export class MapsModal {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController) {
  }

  dismissMapsModal()
  {
    this.viewCtrl.dismiss();
  }

}
