import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StartTrapPage } from './start-trap';

@NgModule({
  declarations: [
    StartTrapPage,,
  ],
  imports: [
    IonicPageModule.forChild(StartTrapPage,),
  ],
  exports: [
    StartTrapPage,
  ]
})
export class StartTrapModule {}
