import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import {FormBuilder, FormGroup, ValidatorFn, AbstractControl, Validators } from '@angular/forms';

import { ControlTemplatePage } from '../control-template/control-template'
import { MapsModal } from '../maps-modal/maps-modal';

import { StateRegister } from '../../class/stateRegister';

@IonicPage()
@Component({
  selector: 'page-start-trap',
  templateUrl: 'start-trap.html',
})
export class StartTrapPage {

  statesRegisters: StateRegister[];
  trapType: number;
  formValidate: FormGroup;
  formBuilder: FormBuilder;
  numberIdTrap: number;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController, 
              private modalCtrl:ModalController) {
    this.statesRegisters = this.navParams.get('statesRegisters');
    this.trapType = Number(localStorage.getItem('id_trap_type'));
    let minNumberIdTrap = this.statesRegisters[0].trap_number;
    let maxNumberIdTrap = this.statesRegisters[(this.statesRegisters.length)-1].trap_number;
    this.formBuilder = new FormBuilder();
    this.formValidate = this.formBuilder.group({
      validateIdTrap: ['', Validators.compose([minValueNumber(minNumberIdTrap), maxValueNumber(maxNumberIdTrap), Validators.required])]
    });     
  } 

  toTemplateControl()
  {    
    this.navCtrl.push(ControlTemplatePage, {
      idStartTrap: this.numberIdTrap,
      statesRegisters: this.statesRegisters
    });
  }

  openMapsModal()
  {
    let mapsModal = this.modalCtrl.create(MapsModal);
    mapsModal.present();
  }

  setBackgroundColor(){
    let classes;
    if (this.trapType == 1) {
      classes = {
        ceboTemplate: true,
        tcvTemplate: false
      }  
    } else {
      classes = {
        ceboTemplate: false,
        tcvTemplate: true
      } 
    }
    return classes;
  }

  setBackgroundToolbar(){
    if (this.trapType == 1) {
      return 'cebo';
    }
    return 'tcv';
  }
}

export function maxValueNumber(max: Number): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    const input = control.value,
          isValid = input > max;
    if(isValid) 
        return { 'maxValue': {max} }
    else 
        return null;
  };
}

export function minValueNumber(min: Number): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    const input = control.value,
          isValid = input < min;
    if(isValid) 
        return { 'minValue': {min} }
    else 
        return null;
  };
}

