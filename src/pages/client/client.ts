import { Component } from '@angular/core';
import { DatePipe } from '@angular/common';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { TemplateTypePage } from '../template-type/template-type'; 

import { ClientService } from '../../service/client.service';
import { MonitorService } from '../../service/monitor.service';
import { Client } from '../../class/client';

@IonicPage()
@Component({
  selector: 'page-client',
  templateUrl: 'client.html',
})
export class ClientPage {
  clients: Client[];
  current_date: Date;
  rutUser;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private clientSrv: ClientService, 
              private monitorSrv: MonitorService) {
    this.current_date = new Date(2017,9,21);
    let rut = localStorage.getItem('rut_session');
    this.loadAllClients(rut);           
  }

  loadAllClients(rut: string)
  {    
    var datePipe = new DatePipe('short');
    var date = datePipe.transform(this.current_date, "yyyy-MM-dd");        
    this.clientSrv.getClientsPerDay(rut,date)
      .subscribe(
        rs => this.clients = rs,
        er => console.log(er),
        () => console.log(this.clients)
      )       
  }

  toTemplateTypePage(client: Client)
  {
    localStorage.setItem("name_client", client.name);
    localStorage.setItem("physical_place_client", client.physical_place);
    this.navCtrl.push(TemplateTypePage,{
      client: client,
      current_date: this.current_date
    });
  }
}
