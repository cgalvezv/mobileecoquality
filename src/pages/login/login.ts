import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController, LoadingController } from 'ionic-angular';
import { AuthService } from '../../service/auth.service';

import {ClientPage} from '../client/client';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  rut: string;
  password: string;
  isLogged: boolean;
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private menu: MenuController,              
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController,
              private auth: AuthService) {
      this.menu.swipeEnable(false);
  }

  SignUp() {
    let credentials = {rut: this.rut, password: this.password};
    this.auth.login(credentials)
      .subscribe(
        rs => this.isLogged = rs,
        er => console.log(er),
        () => {
          if (this.isLogged) {
            this.loadingLogin();
            this.navCtrl.setRoot(ClientPage)
              .then(data => console.log(data), error => console.log(error));                           
          } else {
            this.deniedAccess();
          }
        }
      )    
  }


  changePassword() {
    let alert = this.alertCtrl.create({
      title: 'Cambiar Contraseña',
      subTitle: 'Reingrese nueva contraseña.',
      inputs: [
        {
          name: 'Email',
          placeholder: 'Contraseña',
          type: 'password'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Cambiar',
          handler: data => {
            this.successChangePassword();
            console.log('Change password clicked');
          }
        }
      ]
    });
    alert.present();
  }

  successChangePassword() {
    let alert = this.alertCtrl.create({
      title: 'Cambiar Contraseña',
      subTitle: 'Su plantilla se ha enviado satisfactoriamente.',
      buttons: ['Ok']
    });
    alert.present();
  }

  deniedAccess() {
    let alert = this.alertCtrl.create({
      title: 'Iniciar Sesión',
      subTitle: 'Ha ingresado rut de usuario o contraseña incorrecto(s).',
      buttons: ['OK']
    });
    alert.present();
  }

  loadingLogin() {
    let loader = this.loadingCtrl.create({
      content: "Iniciando sesión...",
      duration: 3000
    });
    loader.present();
  }
}
