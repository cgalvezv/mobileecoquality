import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ControlTemplatePage } from './control-template';

@NgModule({
  declarations: [
    ControlTemplatePage,
  ],
  imports: [
    IonicPageModule.forChild(ControlTemplatePage),
  ],
  exports: [
    ControlTemplatePage
  ]
})
export class ControlTemplateModule {}
