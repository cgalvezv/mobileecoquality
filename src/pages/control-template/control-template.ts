import { Component } from '@angular/core';
import { IonicPage, 
        NavController, 
        NavParams, 
        ModalController, 
        Platform, 
        ActionSheetController, 
        AlertController, 
        LoadingController
      } from 'ionic-angular';
import { DatePipe } from '@angular/common';

import { MapsModal } from '../maps-modal/maps-modal';
import { ClientPage } from '../client/client';
import { StateRegisterPage } from '../state-register/state-register'
import { StateRegister } from '../../class/stateRegister';

import { ControlTemplateService } from '../../service/controlTemplate.service';
import { StateRegisterService } from '../../service/stateRegister.service';

@IonicPage()


@Component({
  selector: 'page-control-template',
  templateUrl: 'control-template.html',
})
export class ControlTemplatePage 
{  
  statesRegisters: StateRegister[];
  trapType: number;
  bodyMail: string;
  findings: string;
  actions: string;
  indexStart;
  trapNumberID;

  constructor(public platform: Platform,
              public navCtrl: NavController,
              public navParams: NavParams, 
              private modalCtrl:ModalController,
              private alertCtrl: AlertController,
              private actionsheetCtrl:ActionSheetController,
              private loadingCtrl: LoadingController,
              private controlTemplateSrv: ControlTemplateService,
              private stateRegisterSrv: StateRegisterService,
              private datepipe: DatePipe
            ) 
  {
    this.trapType = Number(localStorage.getItem('id_trap_type'));    
    this.findings = '';
    this.actions = '';      
    this.statesRegisters = this.navParams.get('statesRegisters');   
    this.indexStart = this.navParams.get('idStartTrap');                 
  }

  ionViewWillEnter()
  { 
    this.bodyMail = this.createBodyMail();
    this.loadTrapInfo();     
  }

  updateVisit()
  {    
    let idControlTemplate = localStorage.getItem("id_control_type");
    
    this.stateRegisterSrv.updateStateVisit(Number(idControlTemplate), this.findings, this.actions)
    .subscribe(
      rs => console.log('Updated!!'),
      er => console.log(er),
      () => console.log('')
    )
  }
  getTrapIsChecked(id)
  {    
    for (var index = 0; index < this.statesRegisters.length; index++) {
      if (this.statesRegisters[index].trap_number == id || 
          this.statesRegisters[index].trap_letter+""+this.statesRegisters[index].trap_number == id) 
      {
        return this.statesRegisters[index].is_registered;
      }
    }
    return false;
  }

  toStateRegister(id)
  {       
    this.navCtrl.push(StateRegisterPage, {
      idTrap: id,
      stateRegister: this.getSelectedStateRegister(id)
    });
  }
  
  openMapsModal()
  {
    let mapsModal = this.modalCtrl.create(MapsModal);
    mapsModal.present();
  }

  confirmSending()
  {
    let sendActionSheet = this.actionsheetCtrl.create({
      title: '¿Desea realmente enviar esta plantilla de control?',
      cssClass: 'action-sheets-send',
      buttons: [
        {
          text: 'Enviar',
          role: 'send',
          icon: !this.platform.is('ios') ? 'checkmark': null,
          handler: () => {
            this.bodyMail = this.createBodyMail();
            let mail = {
              subject: 'Plantilla Control',
              mailClient: 'camilogalvezv@gmail.com',
              body: this.bodyMail
            }
            this.sendMail(mail);
            this.loadingSuccessControlTemplate();   
            this.updateVisit();                    
            this.navCtrl.setRoot(ClientPage);
          }
       },
       {
          text: 'Cancelar',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,          
          handler: () => {
            console.log('Cancelar clickeado');
          }
       }
      ]
    });
    sendActionSheet.present();
  }

  successSendTemplate() {
    let alert = this.alertCtrl.create({
      title: 'Enviar Plantilla',
      subTitle: 'Su plantilla se ha enviado satisfactoriamente.',
      buttons: ['Ok']
    });
    alert.present();
  }

  searchTrap(ev) {
    
    this.loadTrapInfo();

    var val = ev.target.value;

    if (val && val.trim() != '') {
      this.trapNumberID = this.trapNumberID.filter((item) => {
        return (item.toString().toLowerCase().indexOf(val.toLowerCase()) > -1); ;
      })
    }
  }

  setBackgroundColor(){
    let classes;
    if (this.trapType == 1) {
      classes = {
        ceboTemplate: true,
        tcvTemplate: false
      }  
    } else {
      classes = {
        ceboTemplate: false,
        tcvTemplate: true
      } 
    }
    return classes;
  }

  setBackgroundToolbar(){
    if (this.trapType == 1) {
      return 'cebo';
    }
    return 'tcv';
  }

  setHideBackButton(){
    if (this.trapType == 1) {
      return false;
    }
    return true;
  }
  
  private loadTrapInfo()
  {
    this.trapNumberID = [];
    if (this.trapType == 1) {      
      for (var index = this.indexStart-1; index < this.statesRegisters.length; index++) {
        this.trapNumberID.push(this.statesRegisters[index].trap_number);
      } 
    } else {
      for (var index = 0; index < this.statesRegisters.length; index++) {
        this.trapNumberID.push(this.statesRegisters[index].trap_letter +''+ this.statesRegisters[index].trap_number);
      }   
    }    
  }

  private getSelectedStateRegister(idTrap): StateRegister
  {        
    for (var index = 0; index < this.statesRegisters.length; index++) {
      let idTrapAux = this.statesRegisters[index].trap_number.toString();
      if (this.trapType == 2) {
        idTrapAux = this.statesRegisters[index].trap_letter + '' + this.statesRegisters[index].trap_number; 
      }               
      if (idTrapAux == idTrap) {        
        return this.statesRegisters[index];
      }
    }
  }

  private loadingSuccessControlTemplate() {
    let loader = this.loadingCtrl.create({
      content: "Enviando plantilla de control...",
      duration: 2000
    });
    loader.onDidDismiss(() => {
      this.successSendTemplate(); 
    });
    loader.present();
  }
  

  private createBodyMail(): string
  {
    let nameTrapType = this.trapType == 1 ? 'CEBOS' : 'TRAMPAS CAPTURA VIVA';
    let visitDate = this.datepipe.transform(localStorage.getItem("visit_date"), "dd/MM/yyyy");
    let descriptionTraps = this.trapType == 1 ? '<p>V : Vacío <br> P : Parcialmente comido <br> D : Desaparecido <br> S : Sin novedad <br> C : Comido por otra especie</p>':
                                                '<p>S : Sin novedad <br> C : Captura <br> E : Extraviada <br> D : Dañada</p>';
    let bodyMail = '';
    let rowNumberControltemplate = '<br><div class="row"><div class="col-xs-6 col-sm-6 col-md-6"></div><div class="col-xs-2 col-sm-2 col-md-2">'+
                  '<p>N° '+ localStorage.getItem("id_control_type") +'</p></div><div class="col-xs-4 col-sm-4 col-md-4"></div></div>';
    let rowTitle = '<br><div class="row"><div class="col-xs-1 col-sm-1 col-md-1"></div><div class="col-xs-3 col-sm-3 col-md-3">'+
                   '<img src="http://www.ecoquality.cl/sistema/service/img/logo_ecoquality.png"></div><div class="col-xs-7 col-sm-7 col-md-7">'+
                   '<br><h3><B>CONTROL '+ nameTrapType +' DESRATIZACIÓN</B></h3></div><div class="col-xs-1 col-sm-1 col-md-1"></div></div><br>';

    let rowInfo = '<div class="row"><div class="col-xs-1 col-sm-1 col-md-1"></div><div class="col-xs-5 col-sm-5 col-md-5"><table border="1">'+
                  '<tbody><tr><td style="width:200px; height:10px;">CLIENTE</td><td style="width:500px; height:10px;">'+ localStorage.getItem('name_client') +'</td></tr>'+
                  '<tr><td>LUGAR</td><td>'+ localStorage.getItem('physical_place_client') +'</td></tr><tr><td>TOTAL DE CEBOS</td><td>'+ localStorage.getItem("total_trap") +
                  '</td></tr><tr><td>FECHA</td><td>'+ visitDate +'</td></tr><tr><td>MONITOR</td><td></td></tr></tbody></table></div><div class="col-xs-1 col-sm-1 col-md-1"></div>'+
                  '<div class="col-xs-3 col-sm-3 col-md-3"><p><u>DESCRIPCIÓN</u></p>'+ descriptionTraps +'</div>'+
                  '<div class="col-xs-1 col-sm-1 col-md-1"></div></div>';        
    let bodyStatesRegisters = '';    
    //Se crea el contenido de la tabla que mostrará los registros de estado en el mail.
    for (var index = 0; index < this.statesRegisters.length; index++) 
    {
      var element = index < this.statesRegisters.length ? this.statesRegisters[index] : null;
      if(this.trapType == 1)
      {
        if ((index + 1) % 40 != 0) {
          bodyStatesRegisters = bodyStatesRegisters + '<tr><td>'+ (index+1) +'</td>'
          if (element != null && element.trap_number == (index+1)) {
            bodyStatesRegisters = bodyStatesRegisters + '<td>'+ this.getStateTrap(element.id_trap_state) +'</td></tr>';
          } else {
            bodyStatesRegisters = bodyStatesRegisters + '<td></td></tr>';
          }
        } else {
          bodyStatesRegisters = bodyStatesRegisters + '<tr><td>'+ (index+1) +'</td>'
          if (element != null && element.trap_number == (index+1)) {
            bodyStatesRegisters = bodyStatesRegisters + '<td>'+ this.getStateTrap(element.id_trap_state) +'</td></tr>';
          } else {
            bodyStatesRegisters = bodyStatesRegisters + '<td></td></tr>';
          }
          bodyStatesRegisters = bodyStatesRegisters + '</tbody>';
          if ((index+1) != this.statesRegisters.length) {
            bodyStatesRegisters = bodyStatesRegisters + '</table></div><div class="col-xs-1 col-sm-1 col-md-1"><table border="1">'+
            '<thead><tr><th><font size="1">N° CEBO</font></th><th><font size="1">ESTADO</font></th></tr></thead><tbody>';
          }
        } 
      }
      else
      {
        if ((index + 1) % 40 != 0) {      
          let tvcStateAux = element.trap_number == 1 ? '' : element.trap_number;    
          bodyStatesRegisters = bodyStatesRegisters + '<tr><td>'+ element.trap_letter+''+ tvcStateAux +'</td>'
          bodyStatesRegisters = bodyStatesRegisters + '<td>'+ this.getStateTrap(element.id_trap_state) +'</td></tr>';
        } else {
          let tvcStateAux = element.trap_number == 1 ? '' : element.trap_number;
          bodyStatesRegisters = bodyStatesRegisters + '<tr><td>'+ element.trap_letter+''+tvcStateAux +'</td>'
          bodyStatesRegisters = bodyStatesRegisters + '<td>'+ this.getStateTrap(element.id_trap_state) +'</td></tr>';
          bodyStatesRegisters = bodyStatesRegisters + '</tbody>';
          if ((index+1) != this.statesRegisters.length) {
            bodyStatesRegisters = bodyStatesRegisters + '</table></div><div class="col-xs-1 col-sm-1 col-md-1"><table border="1">'+
            '<thead><tr><th><font size="1">N° CEBO</font></th><th><font size="1">ESTADO</font></th></tr></thead><tbody>';
          }
        } 
      }           
    }
    //Se crea la definicion de la tabla que mostrará los registros de estado en el mail.
    let rowStatesTable = '<div class="row"><div class="col-xs-1 col-sm-1 col-md-1"></div><div class="col-xs-1 col-sm-1 col-md-1">'+
                         '<table border="1"><thead><tr><th><font size="1">N° CEBO</font></th><th><font size="1">ESTADO</font></th>'+
                         '</tr></thead><tbody>'+ bodyStatesRegisters +'	</tbody></table></div></div>';
    //Se crea el campo donde irán los hallazgos en la plantilla de control.                       
    let rowFindings = '<br><div class="row"><div class="col-xs-1 col-sm-1 col-md-1"></div><div class="col-xs-10 col-sm-10 col-md-10">'+
                      '<p style="max-width:800px;word-wrap:break-word;">Hallazgo: '+ this.findings +'</p></div><div class="col-xs-1 col-sm-1 col-md-1"></div></div>';
    //Se crea el campo donde irán las acciones en la plantilla de control.                   
    let rowActions = '<div class="row"><div class="col-xs-1 col-sm-1 col-md-1"></div><div class="col-xs-10 col-sm-10 col-md-10">'+
                     '<p style="max-width:800px;word-wrap:break-word;">Acciones: '+ this.actions + '</p></div><div class="col-xs-1 col-sm-1 col-md-1"></div></div>';
    let rowSignings = '<br><br><div class="row"><div class="col-xs-2 col-sm-2 col-md-2"></div><div class="col-xs-3 col-sm-3 col-md-3">'+
                      '<p>°V°B Monitor</p></div><div class="col-xs-2 col-sm-2 col-md-2"></div><div class="col-xs-3 col-sm-3 col-md-3">'+
                      '<p>°V°B Supervisor Técnico</p></div><div class="col-xs-2 col-sm-2 col-md-2"></div></div>'
    let rowFooter = '<div class="row"><div class="col-xs-1 col-sm-1 col-md-1"></div><div class="col-xs-1 col-sm-1 col-md-1"><p>F.OPE.02</p>'+
                    '</div><div class="col-xs-3 col-sm-3 col-md-3"></div><div class="col-xs-1 col-sm-1 col-md-1"><p>Versión1</p></div>'+
                    '<div class="col-xs-3 col-sm-3 col-md-3"></div><div class="col-xs-1 col-sm-1 col-md-1"><p>RD.02.07</p></div>'+
                    '<div class="col-xs-1 col-sm-1 col-md-1"></div></div>';

    bodyMail = rowTitle +
               rowInfo +
               rowNumberControltemplate+
               rowStatesTable+
               rowFindings +
               rowActions +
               rowSignings +
               rowFooter;
    return bodyMail;
  }

  private getStateTrap(id: number): string 
  {
    if (id == 1) {
      return 'V';
    }
    if (id == 2) {
      return 'P';
    }
    if (id == 3) {
      return 'D';
    }
    if (id == 4 || id == 6) {
      return 'S';
    }
    if (id == 5) {
      return 'C';
    }
    if (id == 7) {
      return 'C';
    }
    if (id == 8) {
      return 'E';
    }    
    return 'D';
  }

  private sendMail(bodyMail)
  {
    this.controlTemplateSrv.sendMailToClient(bodyMail)
      .subscribe(
        rs => console.log('Sended!!'),
        er => console.log(er),
        () => console.log('')
      )
  }
}

